package examExample;

import examExample.GeneticAlgorithms.GeneticAlgorithm;
import examExample.GeneticAlgorithms.Preset;

public class Main {
    public static void main (String[] args) {

        GeneticAlgorithm geneticAlgorithm = Preset.getDefaultGA();

        geneticAlgorithm.runWithDebugMode();
        geneticAlgorithm.showGraphInWindow();
        geneticAlgorithm.printProperties();
        geneticAlgorithm.printResults();

    }
}